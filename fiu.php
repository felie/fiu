<?php 
/**
 * @copyright  Copyright (c) 2021 François Elie & Marin Elie
 * @license    http://opensource.org/licenses/AGPL-3.0 AGPL-3.0
 * @link       https://gitlab.adullact.org/felie/fiu
 */

/*TODO
  mettre le début de la page web optionnel - rejeté dans l'index ?
/*

/*============================================== indication des erreurs php ====================*/
 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);

session_start();
//echo 'session='.session_id().' dans fiu<br>';

require_once('fiu_util.php');
//fiu_view($_POST);

fiu_start_the_timer('fiu');
require_once('fiu_config.php');
fiu_connection_init(); // because of action in fiu_tools
require_once('fiu_tools.php');

//*============================================== constantes, variables et connexion base de données ====================*/

function fiu_connection_init(){
    global $fiu_pdo,$fiu_host,$fiu_db,$fiu_charset,$fiu_user,$fiu_pass;
    $fiu_dsn = "mysql:host=$fiu_host;dbname=$fiu_db;charset=$fiu_charset";
    //echo "connexion: $fiu_dsn<br>";
    $fiu_options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,       
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    try {
        //fiu_start_the_timer();
        $fiu_pdo = new PDO($fiu_dsn, $fiu_user, $fiu_pass, $fiu_options);
        //echo fiu_duration();
    } 
    catch (\PDOException $fiu_e) {
        throw new \PDOException($fiu_e->getMessage(), (int)$fiu_e->getCode());
    }
}

/*==========================xmp==================== utilities ====================*/

bindtextdomain("fiu", "./locale");

function error($s){
    echo "<div style='color:red;'>ERROR $s</div><br/>";
}

function dump($var):void{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
}

function show($var,$nom=''):void{
    echo "<br/><b>$nom</b><pre><xmp>";
    print_r($var);
    echo "</xmp></pre>";
}

function stripquote($s){
    if ($s[0]=="'")
        $s=substr($s,1,strlen($s)-2);
    return $s;
}   

gettext('update'); // for poedit - because nowherer there an _('update')...
gettext('create');

/*
Turn a string into an array by following simple construct rules such as proper use of bracket
*/
function str2array($s){ // from return array (recursive)
    $r=[];
    $temp='';
    $prof=0;
    if ($s[0] =='[' and $s[strlen($s)-1]==']'){
        if (strlen($s)>2){
            foreach (str_split(substr($s,1,-1)) as $c){
                if (in_array($c,['[','(','{']))
                    $prof++;
                else 
                    if (in_array($c,[')','}',']']))
                        $prof--;
                if ($prof != 0){
                    $temp.=$c;
                }else{
                    if ($c == ','){
                        $r[]=str2array(stripquote($temp));
                        $temp='';
                    }else
                        $temp.=$c;
                }

            }
            $r[]=str2array(stripquote($temp));
            return $r;
        }else
            return([]);}
    else
        return $s; 
}

$fiu_envelop=[
'default_envelop' => [
"<table class='fiu_table sortable'>\n%s\n</table>\n", 
"  <tr>\n%s  
  </tr>\n",
"   <td>&nbsp;
       %s
   </td>\n",
"    <th>
       %s
    </th>\n"
],
'form_envelop' => [ 
    '<form method="post" enctype="multipart/form-data">
<fiu_HIDDEN_DATA>
<input type="hidden" name="<fiu_MOD>" value="whatever"/>
 <table class="fiu_table">%s
  <tr class="noborder rightalign">
   <td class="noborder" style="background-color:inherit;" colspan=100%%>
    <input style="font-weight:bold;" type="submit" size=1 value="<fiu_MOD>"/>
   </td>
  </tr>
 </table> 
 </form>',
    '<tr>%s
  </tr>',
    '<td>%s
</td>',
    '<th>%s
  </th>'
],
'form_envelop_ajax' => [ 
    '<form method="post" enctype="multipart/form-data" onsubmit=\'send_data(this);event.preventDefault();return false;\'">
<fiu_HIDDEN_DATA>
<input type="hidden" name="<fiu_MOD>" value="whatever"/>
 <table class="fiu_table">%s
  <tr class="noborder rightalign">
   <td class="noborder" style="background-color:inherit;" colspan=100%%>
    <input style="font-weight:bold;" type="submit" size=1 value="<fiu_MOD>"/>
   </td>
  </tr>
 </table> 
 </form>',
    '<tr>%s
  </tr>',
    '<td>%s
</td>',
    '<th>%s
  </th>'
]
];

$fiu_predefmask = [
    '-' => '%s',
    'h' => '<!--%s-->',
    'c' => '<center>%s</center>',
    'simple link' => '<a href="%1$s">%1$s</a>',
    'link' => '<a href="%s">%s</a>',
    'b' => '<b>%s</b>',
    'comment' => '<!-- %s -->',
    'option' => '<option value="%s">%s</option>',
    'u' => '<a href="?id=%s&fiu_action=update&table=<fiu_TABLE>"><img src="/ai/images/edit.png"></a>',
    'd' => '<a href="?id=%s&fiu_action=delete&table=<fiu_TABLE>" onclick="return(confirm(\'Confirmation de l\'effacement de l\'enregistrement n°<fiu_ID> ?\'))"><img src="/ai/images/del.png"/></a>',
    'ud' => '<a href="?id=%1$s&fiu_action=update&table=<fiu_TABLE>"><img src="/ai/images/edit.png"></a><a href="?id=%1$s&fiu_action=delete&table=<fiu_TABLE>" onclick="return(confirm(\'Confirmation de l\\\'effacement de l\\\'enregistrement n°%1$s ?\'))"><img src="/ai/images/del.png"/></a>',
    'stars' => '%s<div class="star-rating">
          <input type="radio" name="<fiu_FIELD>[<fiu_ID>][]" value="5" id="id<fiu_TOKEN>-5">
          <label for="id<fiu_TOKEN>-5">★</label>
          <input type="radio" name="<fiu_FIELD>[<fiu_ID>][]" value="4" id="id<fiu_TOKEN>-4">
          <label for="id<fiu_TOKEN>-4">★</label>
          <input type="radio" name="<fiu_FIELD>[<fiu_ID>][]" value="3" id="id<fiu_TOKEN>-3">
          <label for="id<fiu_TOKEN>-3">★</label>
          <input type="radio" name="<fiu_FIELD>[<fiu_ID>][]" value="2" id="id<fiu_TOKEN>-2">
          <label for="id<fiu_TOKEN>-2">★</label>
          <input type="radio" name="<fiu_FIELD>[<fiu_ID>][]" value="1" id="id<fiu_TOKEN>-1">
          <label for="id<fiu_TOKEN>-1">★</label>
        </div>',
    'range' => '<div class="range-wrap"><input type="range" name="<fiu_FIELD>[<fiu_ID>][]" value="%1$s" class="range"><span class="bubble">%1$s</span></div>',
    'f' => '<input type="file" name="<fiu_FIELD>[<fiu_ID>][]" value="%1$s"> %1$s',
    'file' => '<input type="file" name="<fiu_FIELD>[<fiu_ID>][]" value="%1$s"> %1$s',
    'files' => '<input type="file" name="<fiu_FIELD>[<fiu_ID>][]" value="%1$s" multiple> %1$s',
    'image' => '<img style="max-width:100px;max-height:100px" src="%s" alt=""/>',
    'i' => '<img style="max-width:100px;max-height:100px" src="%s" alt=""/>',
    'squarethumb' => '<img style="border:0px;min-width:100px;min-height:100px;max-width:100px;max-height:100px" src="%s" alt=" "/>',
    'getimage' => '<input type="file" name="<fiu_FIELD>[<fiu_ID>][]" value="%1$s"><img style="max-width:100px;max-height:100px" src="%1$s" alt=" "/><input type="hidden" name="<fiu_FIELD>default[<fiu_ID>]" value="%1$s">',
    'getimages' => '<input type="file" name="<fiu_FIELD>[<fiu_ID>][]" value="%1$s" multiple><ul>list()</ul>|<li><img style="max-width:100px;max-height:100px" src="%1$s" alt=" "/></li>name="<fiu_FIELD>default[<fiu_ID>]" value="%1$s">',
    'images' => '<div class="gallery cf">list()</div>|<div><img src="%1$s" alt=" "/></div>'
];

/*======================================================= actions =====================================*/

if (isset($_GET['fiu_action'])){
	switch ($_GET['fiu_action']){
		case 'update': 
            echo '<h1>UPDATE</h1>';
            echo fiu_sql2html('SELECT * FROM '.$_GET['table'].' WHERE '.$_GET['table'].'.id ='.$_GET['id'],'update');
            break;
		case 'delete': 
            $fiu_q='DELETE FROM '.$_GET['table'].' WHERE id ='.$_GET['id'];
            echo "$fiu_q";
            fiu_query(($fiu_q));
            //$fiu_pdo->query($fiu_q);
	}
}

/*======================================================= functions ===================================*/

/*
count the number of argument needed for a given format 
*/
function fiu_size_of_mask($format):int{
	// cut the format into groups to analyse it 
    preg_match_all('/%(?:(\d+)[$])?[-+]?(?:[ 0]|[\'].)?(?:[-]?\d+)?(?:[.]\d+)?[%bcdeEufFgGosxX]/',$format,$m); 
    $max = 0;
	// check the 1st group which is the one containing the id of the argument used
    for($i=0;$i<count($m[1]);$i++){
	// if null use the next id (starting from 1 and increasing regardless of the id used like in '%2$s')
        if($m[1][$i] == ""){
            $max++;
            $m[1][$i] = strval($max);
        }
    }
    return count(array_unique($m[1]));

}   

if (isset($_GET['fiu_token']))
    $fiu_token = $_GET['fiu_token'];
else
    $fiu_token = 0;

//echo "fiu_token=$fiu_token<br>";

$fiu_recurs=0;  
if (isset($_GET['recurs'])) // for recursive modal
    $fiu_recurs = $_GET['recurs']; //

function listbox_options($target_field){
    $result = fiu("select id,$target_field from $target_field"."s order by $target_field",'select',["<option value='%s'>%s</option>"],'h',['%s','%s','%s']);
    //echo "*".$result."*";
    if (!str_contains($result,"<option value=NULL></option>"))
        $result = '<option value=NULL></option>'.$result;
    return $result;
}

function fiu_automask($mod,$field,$type,$f,$stock,&$fiu_args_to_modify,$pointer,$table,$jointarget):string{ //GENERATION DES MASQUES AUTOMATIQUE
    global $fiu_predefmask,$fiu_recurs,$fiu_recursive_add;
    if (isset($pointer[$f])){
        $name=$pointer[$f];
        //echo "f=$f name=$name<br/>";
        preg_match('/^([^.]*)s\.(.*)$/',$name,$r);
    }else{
        $name=$f;
    }
    $s = "%s"; // default;
    // traitement des masques automatiques par nom de champ
    //echo "<br/>mod=$mod<br/>";
    if ($mod=='select'){
        if ($f=='image')
            $s=$fiu_predefmask['image'];
        if ($f=='images')
            $s=$fiu_predefmask['images'];
        if ($f=='url' or $f=='link')
            $s=$fiu_predefmask['simple link'];
    }else{ // pour la mise  jour, on ne peut pas mettre à jour l'image autrement qu'à la main
        if ($f=='image')
            $s=$fiu_predefmask['getimage'];
        if ($f=='images')
            $s=$fiu_predefmask['getimages'];
    }
    //echo "$s<br/>";
    if($mod!="select" and $s=='%s'){
        $stock +=1;
        //echo "field=$field<br/>";
        //show($jointarget,'jointarget in automask');
        //echo "dans automask type=$type<br/>";
        switch($type){
            case (preg_match('/^id\_(.*)$/',$type,$options) ? true : false) : // cas des champs clé étrangères id_truc, id_truc_machin
                $x=explode('_',$type);
                //echo "type=$type<br/>";
                $target_field=$x[1];
                //echo "target field=$target_field<br/>";
                $name = $type; // cas particulier pour le champ à modifier
                //echo "SELECT id,$target_field FROM $target_field"."s order by $target_field";
                // for recursive modal window
                //$fiu_id++; //*** */
                $modal_call='';
                if ($fiu_recursive_add){ // before: nested
                    $modal_call="<div id='[DIV_ID]' class='button_add pseudo-button' 
                    onclick=\"modal(event,'$target_field',(this.id)-1,[DIV_ID]);\">+</div>";

                }
                $s = "%s
                <div id='[DIV_ID]' class='$name'>
                  <div id='[DIV_ID]' field='$target_field' name='$name' class='reload$target_field' style='display:inline'>
                    <select id='[DIV_ID]' name=\"$name"."[<fiu_ID>][]\">" 
                      //.fiu_sql2html("SELECT id,$target_field FROM $target_field"."s order by $target_field",'select',[$fiu_predefmask['option']],'v',['%s','%s','%s'],[],0).
                      .listbox_options($target_field).
                    "</select>  
                    $modal_call
                  </div>
                </div>";
                break;
            case (preg_match('/^int\((.*)\)$/',$type,$r) ? true : false) :
                $s = '<input type="text" name="'.$name.'[<fiu_ID>][]" size="'.$r[1].'" value="%s" />';
                break;
            case 'date':$s = '<input type="date" name="'.$name.'[<fiu_ID>][]" value="%s" />';
                break;
            case 'text':
            case 'mediumtext':
            case 'longtext':
                $s = '<textarea rows="4" cols="15" name="'.$name.'[<fiu_ID>][]" value="%1$s">%1$s</textarea>';
                break;
            case 'tinytext':
                $s = '<input type="text" name="'.$name.'[<fiu_ID>][]" size="15" value="%s"/>';
                break;
            case (preg_match('/^varchar\((.*)\)$/',$type,$r) ? true : false) :
                $s = '<input type="text" name="'.$name.'[<fiu_ID>][]" size="'.$r[1].'" value="%s" />';
                break;

            case (preg_match('/^set\((.*)\)$/',$type,$options) ? true : false) :
                $s = '%s';
                foreach(explode(',',$options[1]) as $option){
                    $option = str_replace("'","",$option);
                    $s .= '<input type="checkbox" name="'.$name.'[<fiu_ID>][]" value="'.$option.'">'.$option.'<br>';
                }
                break;

            case (preg_match('/^enum\((.*)\)$/',$type,$options) ? true : false) :
                $s = '%s';
                foreach(explode(',',$options[1]) as $option){
                    $option = str_replace("'","",$option);
                    $s .= '<input type="radio" name="'.$name.'[<fiu_ID>][]" value="'.$option.'">'.$option.'<br>';
                }
                break;

            default : $s = '(default)%s'; break;
        }
    } 
    if (fiu_size_of_mask($s)>$stock){
        error(_('Too few data for the mask.'));
    }
    else{
        $fiu_args_to_modify[] = $name;
        //echo "<xmp>$s</xmp>";
        return $s;
    }
}

function fiu_cellule_check($cellule):string{
    //echo "<b>cellule AVANT</b>:<xmp>$cellule</xmp><br/>";
    $cellule=str_replace("\n",'SauTdeLigne',$cellule);// sinon le pregmatch ne prend qu'avant le saut de ligne...
    //if(preg_match('/^([^<]*)(<input type="(?:checkbox|radio)".*)$/',$cellule,$a)){
    if(preg_match('/^([^<]*)(.*)(<input type="(?:checkbox|radio)".*)/',$cellule,$a)){
        //echo "je suis dedans";
     //   echo "avant : $cellule <br/>";
        $os=explode(',',$a[1]);
        //show($os,'OS');
        $cellule=$a[2].$a[3];
        foreach($os as $o){
           $cellule = str_replace("value=\"$o\"", "value=\"$o\" checked ",$cellule);
        }
      //  echo "après : $cellule <br/>";
      $cellule=str_replace('SauTdeLigne',"\n",$cellule);
      //echo "<b>cellule APRES</b>:<xmp>$cellule</xmp><br/>";
        return $cellule;
    }
    //echo "<hr/>*1*$cellule***";
    //echo "<hr/>*2*<xmp>$cellule</xmp>***";
    $cellule =  preg_replace('/^([^<]+)(.*)>\1/','\2 selected>\1',$cellule); // pour les select
    //$cellule =  preg_replace('/^([^<]+)(.*)value="\1"/','\2value="\1" checked',$cellule); // pour 
    //echo "<hr/>*3*<xmp>$cellule</xmp>***";
    $cellule=str_replace('SauTdeLigne',"\n",$cellule);
    //echo "<b>cellule APRÈS</b>:<xmp>$cellule</xmp><br/>";
    return $cellule;
}

function fiu_get_query_args($q):array{ //RECUPERATION DES INFORMATIONS RELATIVES AUX TABLES DE REQUETE SQL
    global $fiu_pdo;
    //fiu_start_the_timer('all');
    $x = rand(1,100000); // to avoid conflict between "mv"

    $data = fiu_query("DROP TABLE IF EXISTS mv$x;CREATE TEMPORARY TABLE mv$x AS $q;DESCRIBE mv$x");
    fiu_query("DROP TABLE mv$x");
    
    //$fiu_pdo->query("DROP VIEW IF EXISTS mv");
    //echo "Q=$q<br>";
    //$fiu_pdo->query("CREATE VIEW mv AS $q");
    //$data = $fiu_pdo->query("DESCRIBE mv"); // mv
    //echo fiu_duration('all');
    $result=[];
    while ($line=$data->fetch()){
        $result[]=$line;
    }
    //show($result,'result dans fiu_get_query_args');
    //fiu_view($result,$q);
    return $result;
}

function listbr($q){ // return first column with <br/> separator (for vertical writing in <hr>  
    global $fiu_pdo;
    //$data = $fiu_pdo->query($q);
    $data = fiu_query($q);
    $r = [];
    while ($line = $data->fetch(PDO::FETCH_NUM)){
        $r[] = $line[0];
    }
    return join('<br/>',$r);
}
    
function fiu_flip($arr):array{ //INVERSION DES LIGNES ET COLONNES D'UN TABLEAU BIDIMENSIONNEL
    $out = array();
    //show($arr,'arr');
    foreach ($arr as $key => $subarr){
        foreach ($subarr as $subkey => $subvalue){
            $out[$subkey][$key] = $subvalue;
        }
    }
    return $out;
}

function fiu_request2ai(&$q,&$arg_field_arr,&$pointer,$mod,$table1,&$jointarget){ // Traite la requête pour gérer les normes spécifiques d'ai
    // "duck.naissance>villes.fondateur>gugusse.nom" transformation en jointure
    // select ...  ALIAS1.nom ... join gugusse as ALIAS1 on ALIAS2.fondateur=ALIAS1.id join ville as ALIAS2 on duck.naissance=ALIAS2.id 
    global $fiu_pdo,$fiu_token,$fiu_label_ala_lisp;
    
    //echo "q à l'entrée de request2ai=$q<br/>";
    preg_match_all('/^SELECT (.*) FROM ([^, ]*).*$/',$q,$arg_field_arr);
    //show($arg_field_arr,'show_field_arr');
    $t1=$arg_field_arr[2][0];
    //echo "t1=$t1";
    $arg_field_arr=explode(',',$arg_field_arr[1][0]);
    
    foreach ($arg_field_arr as &$c){ // transforme les id_truc (clés étrangères) en a.b>c.d
    	if (substr($c,0,3) == 'id_'){
        	$x=explode('_',$c);
                $c="$t1.$c>".$x[1].'s';
                if (sizeof($x)<4){
                    $c.=".".$x[1];
                }else{
                    $c.=".".$x[3];
                }
        }
    }
    //show($arg_field_arr,'arg_field_arr');

    // on calcule la liste des champs de la table, on en aura besoin à la fin
    //$data = $fiu_pdo->query("select distinct(COLUMN_NAME) from information_schema.COLUMNS where TABLE_NAME='$t1';");
    $data = fiu_query("select distinct(COLUMN_NAME) from information_schema.COLUMNS where TABLE_NAME='$t1';");
                while ($line = $data->fetch(PDO::FETCH_NUM)){
                $listedeschamps[] = $line[0];
            }
    //show($listedeschamps);

    foreach($arg_field_arr as &$arg){
        
        
        
        //echo "arg=$arg<br/>";
        $arg_arr = explode('>',$arg); // attention si on a des comparaisons dans une une fonction.
        $firstw = explode('.',$arg_arr[0])[0];
        //echo "firstw=$firstw<br/>";
        //show($arg_arr,'arg_arr');
        // pour éviter ce cas, il faut que le premier terme soit la table1
        // A.B>C.D>E.F  select A1.F      JOIN C as A1 ON A.B=A1.id
        //                               JOIN E as A2 on A1.D=A2.id 
        $join = null;
        $f=[];
        if (count($arg_arr)!=1 and ($firstw == $t1)){ // si le découpage par > est ok pour des champs
            //show($arg_arr,'arg_arr');
            $queue=explode('.',end($arg_arr));
            $queue=$queue[1];
                        // field to show
            for($i=0;$i<count($arg_arr)-1;$i++){
                $a=explode('.',$arg_arr[$i]);
                $f[]=$a[1];
                $a1=explode('.',$arg_arr[$i+1]);
                if($i == 0){
                    $aliasfirst = $a[0];
                }else{
                    $aliasfirst = 'ALIAS'.($fiu_token-1);
                }
                $join .= ' LEFT JOIN '.$a1[0]." as ALIAS$fiu_token ON $aliasfirst.$a[1] = ALIAS$fiu_token.id ";
                $jointarget[$fiu_token] = $a1[0]; // alias<-->jointure table
                $fiu_token++;
            }
            //show($arg,'arg');
            //show($join,'join');
            //show($f,'f');
            $f[] = $queue;
            $n=$f[0];
            if ($fiu_label_ala_lisp){
                $n = $f[$i]."(".$n.")";
            }else{
                $n=explode('_',$f[0]);
                $n=$n[sizeof($n)-1];
            }
            //echo "le nom est: $n<br/>";
            if ($mod!='select'){
                $name=explode('.',$arg_arr[0]);
                $pointer[]=$name[1];
                //$pointer["ALIAS".($fiu_token-1)]=$arg_arr[0];
            }else{
                $pointer[]=$arg;
            }
            $arg="ALIAS".($fiu_token-1).".$queue as '$n' ";
        }
        else { // ajout éventuel de la table1 pour éviter les ambiguités
            $pointer[]=$arg;
            $arg_arr = explode('.',$arg);
            //if (count($arg_arr)==1){
            // met la table en regard sauf si c'est une fonction comme sum,group_concat, etc...
            //echo "table=$t1<br/>";
            if (in_array($arg,$listedeschamps)) // seulement les champs, parce qu'il peut y avoir des fonctions
                $arg="$t1.$arg";               // attention s'il y avait plusieurs tables!
            //}
        }
    //show($arg_field_arr,'arg_field_arr à la fin');
    $q=preg_replace('/^(SELECT ).*( FROM [^ ]*)(.*)$/','$1'.join(',',$arg_field_arr)."$2$join$3",$q);
    }
    //echo "$q à la fin de request2ai<br/>";
}

function fiu_develop_star(&$q,$table1,$mod){ // in case of *, develop and in case of id_truc convention, transform in a.b>c.d conventions, and remove id 
    global $fiu_pdo;
     // cas du select * qu'il faut développer
    $args = fiu_get_query_args($q,$fiu_pdo);
    $fields = array_column($args,"Field");
     if ($mod == 'insert'){
         array_shift($fields); // remove the id        
     }
    $fields = implode(',',$fields);
    //echo "FIELDS=$fields<br/>";
    $q=str_replace('SELECT *','SELECT '.$fields,$q);
    return $q;
}

function fiu_xmp($t){ // string or string array
  if (is_array($t))
    if ($t==[])
      return '[]';
    else
      return "['".str_replace(array('<','>'),array('&lt;','&gt;'),implode("','",$t))."']";
  else
    return "'".$t."'";
}

/* to paginate the function fiu_sql2html */

function fiu_arg_to_pass($name,$value){
    return "<input type='hidden' name='$name' value='".serialize($value)."'/>\n"; 
    }

function fiu_sql2html_pagin($page,$q,$mod='select',$m=[],$o=fiu_DEFAULT_ORIENTATION,$e='default_envelop'){
global $fiu_token;
    $fiu_token++;
    $token=$fiu_token;
    $q = fiu_upper_query($q);
    preg_match('/^SELECT (.*?)(FROM.*?)(LIMIT(.*?))?(OFFSET(.*?))?$/',$q,$r);
    //show($r);
    if (isset($r[3])){
        $limit = $r[4];
    }else{
        //echo "SELECT count(*) $r[2]<br/>";
        $result = fiu_simple_query("SELECT count(*) $r[2]");
        $limit = $result[0];
    }   
    if (isset($r[5])){
        $offset = $r[6];
    }else{
        $offset = 0;
    }
    $q = "SELECT $r[1]$r[2] LIMIT $page OFFSET $offset";
    //alert(document.getElementById('offsetmodifcible$fiu_token').innerHTML);s
return "<div class='pagin'><center><form style='display:inline' method='post' action='' onclick=\"submitForm(this,'cible$token');sorttable.init;\">\n".
  fiu_arg_to_pass('q',$q).
  fiu_arg_to_pass('mod',$mod).
  fiu_arg_to_pass('m',$m).
  fiu_arg_to_pass('o',$o).
  fiu_arg_to_pass('e',$e).
  "<input type='hidden' name='pagination'/>
  <input type='hidden' value='$token' name='token' />
  <input type='hidden' value='$offset' name='offsetinitial'/>
  <input type='hidden' value='$limit' name='limit' />
  <input type='hidden' value='$page' name='page' />
  <!--<input type='button' value='|<'/>
  <input type='button' value='<<'>
  <input type='button' value='<'>-->
  <input class='button_pagin' id='cible$token-b1' type='button' width=0 b='|<'>
    <label for='cible$token-b1'><img class='image_pagin' src='../fiu/images/1.png' width=20/></label>
  <input class='button_pagin' id='cible$token-b2' type='button' width=0 b='<<'>
    <label for='cible$token-b2'><img class='image_pagin' src='../fiu/images/2.png' width=20/></label>
  <input class='button_pagin' id='cible$token-b3' type='button' width=0 b='<'>
    <label for='cible$token-b3'><img class='image_pagin' src='../fiu/images/3.png' width=20/></label>
  <input class='button_pagin' id='cible$token-b4' type='button' width=0 b='>'>
    <label for='cible$token-b4'><img class='image_pagin' src='../fiu/images/4.png' width=20/></label>
  <input class='button_pagin' id='cible$token-b5' type='button' width=0 b='>>'>
    <label for='cible$token-b5'><img class='image_pagin' src='../fiu/images/5.png' width=20/></label>
  <input class='button_pagin' id='cible$token-b6' type='button' width=0 b='>|'>
    <label for='cible$token-b6'><img class='image_pagin' src='../fiu/images/6.png' width=20/></label>
  </form><div style='margin-top:-16px;' id='cible$token'>".fiu($q,$mod,$m,$o,$e).
  "<div style='display:none' id='offcible$token'>$offset</div></div></center></div>"; 
}


/*============================================== fonction principale ====================*/

//fiu is an alias for those who are very very fiu! 
function fiu_sql2html(){
    return call_user_func_array("fiu", func_get_args());
}

function fiu_upper_query($q){
    return preg_replace(['/[S|s][E|e][L|l][E|e][C|c][T|t] /','/[F|f][R|r][O|o][M|m] /','/[L|l][I|i][M|m][I|i][T|t] /'],
                    ['SELECT ','FROM ','LIMIT '],$q);
}

function fiu($q,$mod='select',$m=[],$o='h',$e='default_envelop',$th=[],$display=0){ //FONCTION PRINCIPALE
    global $fiu_pdo,$fiu_predefmask,$fiu_debug,$fiu_demo_mode,$fiu_info,$fiu_envelop,$fiu_token,$fiu_nb,$fiu_recurs;
    $fiu_nb++;
    $initial_q=$q;
    $initial_m=$m;
    $initial_e=$e;
    if ($e == [])
        $e = 'default_envelop';
    // intule de continuer s'il n'y a aucune réponse dans la base
    //echo "q=$q<br>";
    $q = fiu_upper_query($q);
    //echo "q=$q<br>";
    $test=preg_replace('/^SELECT (.*?) FROM ([^ ]*) (.*)$/',"SELECT count(*) FROM $2",$q);
    //echo "test=$test<br>";
    if (!($mod=='insert') & fiu_simple_query($test)[0]==0) // no update or select on a empty result
        return _('');
   
    if (!is_array($m)){
        $m=str_split($m,1); //convertit en un tableau de caractères - pour aller plus vite à écrire les tableaux de masques
    }
    if (!is_array($e)){
        //show($e,'enveloppe');
        if ($mod!='select' and $e == 'default_envelop'){
            if ($fiu_recurs == 0)
                $e = 'form_envelop';
            else    
                $e = 'form_envelop_ajax'; // form in recursive modal 
        }
        $e=$fiu_envelop[$e];
    }
    //show($m,'masques à l\'entrée de fiu_sql2html');
    $table1 = preg_replace('/^SELECT(.*)FROM ([^ ]*)(.*)$/','$2',$q);
    $realfields=[];
    if (preg_match('/^SELECT \*(.*)$/',$q)){
        $q = fiu_develop_star($q,$table1,$mod,$realfields); // $realfields comme l'indique son nom (id_ville par ex)
    }
    if ($mod == 'update'){
        if (!preg_match('/SELECT id,/',$q)){ // ensure an id is given for update
            $q=str_replace('SELECT ','SELECT id,',$q);
            }
        array_unshift($m,$fiu_predefmask['comment']); // comment the id
    }
    //show($m,"après traitement update");
    //echo "after develop_star $q<br/>";
    fiu_request2ai($q,$field,$pointer,$mod,$table1,$jointarget);
    //echo "query after requet2ai 1: <b>$q</b> <hr/>";
    //echo "<b>NEW</b> : $q";
    //fiu_start_the_timer('query_args in sql_html');
    $table1 = preg_replace('/^SELECT(.*)FROM ([^, ]*)(.*)$/','$2',$q);
    $query_arg = fiu_get_query_args($q);
    $arg_field = array_column($query_arg,"Field");
    $arg_type = array_column($query_arg,"Type");
    //echo fiu_duration('query_args in sql_html');
    //show($arg_field,'arg_fields');
    //show($arg_type,'arg_type');
    //show($pointer,'pointer');
/* TODO */
    for ($i=0;$i<sizeof($arg_field);$i++){ // pas de ruse!
         if ($pointer[$i]!=$arg_field[$i]){
             $arg_type[$i]=$pointer[$i];
         }
     }
    //view($query_arg);
    $arg_total = count($query_arg);
    //echo "arg_total=$arg_total";
    $arg_used = 0;
    $m_size = [];
    
    //show($arg_field,'argfield'); // les arguments
    //show($pointer,'pointer');
    //show($arg_type,'arg_type');    
    $tab=[];
    if ($mod=='insert'){ // génération d'un tableau vide
        if (preg_match('/(.*)LIMIT (.*)/',$q,$r)){ //  on récupère la veleur de LIMIT
            $limit = $r[2];
            //echo "LIMIT=$limit";
            $emptyrecord = [];
            foreach ($arg_field as $arg){
                $emptyrecord[$arg]='';
            }
            $tab = array_fill(0,$limit,$emptyrecord);
	    }else{
            error("No limit in INSERT");
        }
    }else{
        //echo "q avant récupération des données = $q<br/>"; 
    	//$data = $fiu_pdo->query($q); // get data from sql
        $tab = fiu_total_query($q);
        //$data = fiu_query($q); // get data from sql
        //while ($line = $data->fetch(PDO::FETCH_ASSOC)){
        //    $tab[] = $line;
        //}
   }
   //show($tab,'tab'); // data
    //show($m,'masques à l\'entrée des traitements préliminaires');
    $fiu_args_to_modify = [];
    //show($m,'masques');
    foreach($m as &$mask){ //Traitement preliminaire des masques
        //echo $mask;
        $cible =explode('.',$field[$arg_used]);
        //show($cible,'cible');
        if (isset($cible[1]))
            $cible = $cible[1]; // le champ cible (pour fiu_arg_to_modify) - n'existe pas pour les fonctions
        $tmp = fiu_size_of_mask($mask);
        if($arg_used==$arg_total){
            error("break arg_used($arg_used)==arg_total($arg_total) - vérifiez vos masques trop gourmands");break;
            }
        //echo "<b>mask</b>=*$mask*<br/>";
        if ($tmp==0){
            if($mask=='*'){
                $mask = fiu_automask($mod,$field[$arg_used],$arg_type[$arg_used],$arg_field[$arg_used],$arg_total-$arg_used,$fiu_args_to_modify,$pointer,$table1,$jointarget); 
                $tmp = fiu_size_of_mask($mask);
                //echo "tmp * =$tmp<br/>";               
            }else{
               if ($mask[0] == '='){ // affectation forcée
                $value=substr($mask,1,strlen($mask)-1);
                $mask = "<input type='hidden' name='<fiu_FIELD>[][]' value='$value'/>$value<!--%s-->";
                $fiu_args_to_modify[] = $cible;
                $tmp = fiu_size_of_mask($mask); 
                //echo "tmp affectation forcée=$tmp<br/>";
            }else{
               if (isset($fiu_predefmask[$mask])){
                    $mask = $fiu_predefmask[$mask];
                    $fiu_args_to_modify[] = $cible; // à tout hasard ce peut être un champ imput
                    $tmp = fiu_size_of_mask($mask);
                    //echo "tmp masque prédéfini= $tmp<br/>";
                }
            }
          }
        }
        if ($mask[0] == '?'){
            //echo "$mask";// traitement des masques conditionnels contenant des masques automatiques ou prédéfinis
            if (preg_match('/\^?\((.*)=(.*)\):(.*):(.*)$/',$mask,$r)){
                $x="\$r = ($r[1]=$r[2]) ? $r[3] : $r[4];";
                //show($r,'r');
                //echo "$mod<xmp>mask1=$mask</xmp><hr/>";
                $mask=str_replace('*',fiu_automask($mod,$field[$arg_used],$arg_type[$arg_used],$arg_field[$arg_used],$arg_total-$arg_used,$fiu_args_to_modify,$pointer,$table1,$jointarget),$mask);
                if ($r[3] != '*' and isset($fiu_predefmask[$r[3]])){
                    $mask=str_replace($r[3],$fiu_predefmask[$r[3]],$mask);
                }
                //echo "r3=*$r[3]*<br/>";
                if ($r[4] != '*' and  isset($fiu_predefmask[$r[4]])){
                    $mask=str_replace($r[4],$fiu_predefmask[$r[4]],$mask);
                }
                //echo "<xmp>mask2=$mask</xmp><hr/>";
                $tmp = (fiu_size_of_mask($mask) / 2); // division par deux car il y a sioui et sinon
                //echo "tmp masque conditionnel=$tmp<br/>";
                //echo "tmp=$tmp<br/>";
            }else{
                error('Erreur de syntaxe dans un masque conditionnel');
            }    
        }else{
           $tmp = fiu_size_of_mask($mask);
           //echo "tmp masque fourni=$tmp pour $mask<br/>";
        }
        //echo "size of mask=$tmp <xmp>$mask</xmp><br/>";
        $m_size[]= $tmp;
        $arg_used += $tmp;
    }
    $m = array_slice($m,0,count($m_size));
    //echo ('masques: <xmp>'.join(',',$m).'</xmp>');
     
    //echo "arg used=$arg_used / $arg_total";
     
    while($arg_used<$arg_total){ //Ajout des masques automatiques
        $m[] = fiu_automask($mod,$field[$arg_used],$arg_type[$arg_used],$arg_field[$arg_used],$arg_total-$arg_used,$fiu_args_to_modify,$pointer,$table1,$jointarget);
        $m_size[] = fiu_size_of_mask(end($m));
        $arg_used += end($m_size);
    }
        //show($m,'masques');
        //show($data,'data');
    $flines = null;
    $s = [];
    if (isset($e[3])){ // si enveloppe avec un champ pour les th
        //show($arg_field,'arg_field');
        $nc=0; // numéro de champ
        $vireleid=0;
        if ($mod == 'update'){
            $vireleid=1;
            $nc=1;
        }
        for ($i=0+$vireleid;$i<sizeof($m);$i++){ 
            //echo "msize = $m_size[$i]<br/>";
            if (isset($th[$i])){
                if ($th[$i]!='h'){ // si le champ n'est pas masqué
                    if (preg_match('/QUERY(.*)\/QUERY/',$th[$i],$r)){ // listes spéciales affichées verticalement
                        //show($r,'résultat de la requete');
                        $th[$i] = str_replace('QUERY'.$r[1].'/QUERY',"<div style='white-space:nowrap;' class='vertical'>".listbr($r[1])."</div>",$th[$i]);
                    }
                $s[] = sprintf($e[3],$th[$i]);
                }
            }
            else
                $s[] = sprintf($e[3],$arg_field[$nc]);
            $nc=$nc+$m_size[$i];
            }
        $flines[] = $s;
    }
    
    //show($m_size,'m_size');
    
    //show($flines,'flines');
           
    $n=0;
    foreach($tab as $line){ //Traitement du tableau
        $i=0;
        $fcells = null;
        //echo show($m,'masques');
        //show($fiu_args_to_modify,'to modifiy');
        foreach($m as $k=>$v){// Pour chaque masque, mise en place des cellules avec les données (de $line)
            if (!($i == 0 and $mod == 'update')){ // on ne prend pas la colonne des id pour les updates
                //echo "<xmp>k=$k v=$v</xmp><br/>   ";
                //echo "<xmp>cell=$cell</xmp><br/>";
                if ($v[0] == '?'){ // traitement des masques conditionnels de la forme ?(condition):si oui:si non
                    if (preg_match('/\?\((.*)=(.*)\):(.*):(.*)$/',$v,$r)){
                        //show($r,'r');
                        //echo " condition=(".$line[$r[1]]." == $r[2]) sioui=$r[3] sinon=$r[4]<br/>";
                        //echo "*".$line[$r[1]]."*".$r[2]."   *<br/>";
                        $condition=($line[$r[1]]==$r[2]); // ???
                        //echo $condition;
                        if ($line[$r[1]]==$r[2]){
                            $masque=$r[3];
                        }else{
                            $masque=$r[4];
                        }
                        //echo "$masque<br/>";
                        //echo "<xmp>cell=$cell</xmp>";
                        $cell = vsprintf($masque,array_slice($line,$i,$m_size[$k]));
                    }else{
                        error("Syntaxe incorrecte dans le masque conditionnel $v");
                    }
                }else{ // the lists traitment // TODO
                    if (preg_match('/^(.*)list\(\)(.*)\|(.*)$/',$v,$r)){ // for one parameter only for the moment
                        $dd = explode("\n",join('',array_slice($line,$i,$m_size[$k])));
                        foreach($dd as &$d){
                            $d=sprintf($r[3],$d);
                        }
                        $dd = join('',$dd);
                        $cell = $r[1].$dd.$r[2];
                    }
                    else{
                        $cell = vsprintf($v,array_slice($line,$i,$m_size[$k])); // given mask
                    }
                }
                $fiu_token++;
                $cell = str_replace('<fiu_TOKEN>',$fiu_token,$cell);
                $cell = str_replace('<fiu_FIELD>',$arg_field[$i],$cell);
                if ($mod=='update'){ 
                    $cell = str_replace('<fiu_ID>',$line['id'],$cell);
                    $cell=fiu_cellule_check($cell);             
                }
                if ($mod=='insert'){
                    $cell=fiu_cellule_check(str_replace('<fiu_ID>','',$cell));             
                }
                if (preg_match('/(.*)<input (.*)/',$cell)){
                    //echo "ligne $n à modifier<br/>";
                }
                if ($v != '<!--%s-->')  // le champ spécial 'h' masque toute la colonne (avec son masque d'enveloppe)
                    $fcells[] = sprintf(str_replace('<fiu_FIELD>',$arg_field[$i],$e[2]),$cell);
            }
            $i += $m_size[$k];
        }
        $flines[] = $fcells;
        $n++;
        //ajout de chaque lignes
    }

    if($o=='v'){//Rotation du tableau
        $flines = fiu_flip($flines);
    }
    
    $ftable = null;
    foreach($flines as $l){//Mise en forme du resultat
        //show($l,'l=');
        $ftable .= sprintf($e[1],join('',$l));
        //ajout des balises de ligne
    }
    $hidden_data=[]; // to give args_to_modify to the forms (update/insert)
    if($mod!='select'){//Installation des ids cachés pour update, et des args_to_modify
        $hidden_data[] = '<input type="hidden" name="fiu_table" value="'.$table1.'"/>';
        foreach($fiu_args_to_modify as &$arg){
            //echo "arg ancien=$arg<br/>";
            if (isset($pointer[$arg])){
                //echo "arg=$arg => ".$pointer[$arg]."<br/>";
                $arg=$pointer[$arg];
                //echo "arg nouveau=$arg<br/>";
            }
            $hidden_data[] = "<input type='hidden' name='fiu_args_to_modify[]' value='$arg'/>";
        }
    }
    // opérateur de répétition (pour les tableaux en Markdown ou en LaTeX)
    $e[0] = str_replace(array('<fiu_NBCOL>','<fiu_HIDDEN_DATA>','<fiu_MOD>'),array(sizeof($m),join('',$hidden_data),_($mod)),$e[0]);
    $e[0] = preg_replace_callback('/REP\((.*),(.*)\)/U',function ($m){
                                                            return str_repeat($m[1],$m[2]);
                                                            },$e[0]);
    $html = sprintf($e[0],$ftable); // result of the request in given envelop
    
    // remplacement des [DIV_ID] par des identifiants uniques
    $html=preg_replace_callback('/\[DIV_ID\]/', 
                            function ($matches) use (&$fiu_token){
                              return $fiu_token++; 
                            }
                            ,$html);
    
    $request_info='';
    //show($initial_e);
    if ($display and $fiu_debug>2){ // display his own parameters at the end
	$request_info.="<p/>Parameters:\n<center><table >
	<tr><td>REQUÊTE</td><td>$initial_q</td></tr>
    <tr><td>MODE   </td><td>$mod</td></tr>
    <tr><td>MASQUES</td><td>".fiu_xmp($initial_m)."</td></tr>
    <tr><td>ORIENTATION</td><td>$o</td></tr>
    <tr><td>ENVELOPPE</td><td>".fiu_xmp($initial_e)."</td></tr> \n</table></center>\n";
    if (isset($_GET['mode']))
        $request_info="<xmp>$request_info</xmp>";
    //echo sprintf('<div style="background:yellow">%s</div><p/>',$request_info);
    }
    //show(func_get_args(),'arguments');
    //echo display_args(func_get_args());
    // _('Function call:').display_args(func_get_args()).'<br/>'
    if ($display and $fiu_debug>2)
        $request_info.=_('Calculated query:')."<br/>\n$q\n<br/><p/>"._('Result herein after:');
    if ($display and $fiu_debug>2)
        $request_info="<br/><div style='background:lightgray;padding:5px;border:1px dotted gray;color:black'>$request_info</div>";
    return $request_info.str_replace(array('<fiu_TABLE>','<fiu_NB>'),array($table1,$fiu_nb),"<br/>$html");
    //ajout des balises de tableau et retour du resultat
}

/*============================================== fonctions spéciales ====================*/

function fiu_exists_table($table){
    $r = fiu_simple_query("SHOW TABLES like '$table'");
    //fiu_view($r);
    return ($r[0] == $table);
}

function fiu_simple_query($q){ // renvoie la première colonne d'une requête
    if ($q == '')
        return;
    $result = fiu_query($q);
    //echo "requête dans simple query=$q<br>";
    return $result->FetchAll(PDO::FETCH_COLUMN);
    /*if (sizeof($r) == 1)
        return $r[0];
    elseif (sizeof($r) == 0)
        return '';
    else 
        return $r;*/
}

function fiu_total_query($q){ // renvoie tout le tableau resultat
    global $fiu_pdo;
    $r = [];
    //$result = $fiu_pdo->query($q);
    $result = fiu_query($q);
    //fiu_view($result);
    foreach($result->FetchAll(PDO::FETCH_ASSOC) as $row){
        $r[]=$row;
    }
    return $r;
}

function fiu_query($query){ // string or array
    global $fiu_pdo,$fiu_metrix;
    //echo 'type de query='.gettype($query);
    //echo "*$query*<hr>";

    if (!is_array($query))
        $query = explode(';',str_replace("\n",'',$query));
    
    // see http://sdz.tdct.org/sdz/les-transactions-avec-mysql-et-pdo.html
    /*try {
        $fiu_pdo->query('START TRANSACTION');
        foreach($query as $q)
            if ($q != ''){
                fiu_start_the_timer(1);
                $r = $fiu_pdo->query($q);
                $d = fiu_duration(1);
                $fiu_metrix[] = [$q,$d];
            }
        $fiu_pdo->query('COMMIT');
    }
    catch (PDOException $e){
        $fiu_pdo->rollback();  // transaction cancellation
        echo error($e->errorInfo[2]);
    }*/
 //fiu_view($query);
    try {
        foreach ($query as $q){
            if ($q != ''){
                fiu_start_the_timer(1);
                //echo "Q dans fiu_query=*$q*<br>";
                $r = $fiu_pdo->query($q);
                $d = fiu_duration(1);
                $fiu_metrix[] = [$q,$d];
                //$r = $fiu_pdo->prepare($q)->execute(); // slower but mot secure
            }
        }
    }
    catch (PDOException $e) {
        echo error($e->errorInfo[2]);
    }
    //show($r,'R dans fiu_query');
    return $r; // return result of last query;
}

function fiu_files_from_form(){ // $_FILES traitement for update and insert
    global $fiu_upload,$fiu_uploaddir,$fiu_dir_correction;
    if (sizeof($_FILES)>0){ // for the uploaded files
        $dir = "$fiu_uploaddir/".$_POST['fiu_table'];
        $web = "$fiu_upload/".$_POST['fiu_table'];
        //echo "répertoires: dir=$dir web=$web<br/>";
        if (!file_exists($dir)){ 
            mkdir($dir);
        }
        foreach($_FILES as $field=>$value){
            foreach ($value['name'] as $num=>$list){
                if (sizeof($list)!=0){
                    $liste=[];
                    for($i=0;$i<sizeof($list);$i++){
                        $file = basename($list[$i]);
                        //echo "fichier=$file<br/>";
                        //echo "tmp=".$value['tmp_name'][$num][$i]."<br/>";
                        if (move_uploaded_file($value['tmp_name'][$num][$i], "$dir/$file")){
                        $liste[]="$web/$file";
                        //show($liste,'liste des fichiers');
                        }
                    }
                    $_POST[$field][$num][$i] = join("\n",$liste); // possibly several files
                    //echo $_POST[$field][$num][$i];
                }
            }
        }
    }
}

function fiu_anti_injection(&$t){
    if (is_array($t)){
        foreach($t as &$e){
            if (is_string($e)){
                $e=str_replace(array('"','\\'),array('\"','\\\\'),$e);
            }else{
                fiu_anti_injection($e);
            }
        }
    }
}

function fiu_update(){ //GENERATION ET EXECUTION DES REQUETES D'UPDATE (ZERO SECURITE)
    global $fiu_pdo,$fiu_uploaddir,$fiu_upload;
    //show($_POST,'post in update');
    fiu_anti_injection($_POST);
    //show($_POST,'post in update after fiu_anti_injection');    
    fiu_files_from_form();
    //show($_POST,'post in update');
    //show($_POST['fiu_args_to_modify'],'arg to modify');
    $pre_request = "UPDATE %s SET %s WHERE id = %s";
    $new_data = fiu_flip(array_slice($_POST,3,count($_POST)-3)); // sauf fiu_table, fiu_args_to_modify et fiu_<mod>
    //show($new_data,'newdata');
    foreach ($new_data as $id => $record){  
        $modifs= [];
        foreach ($_POST['fiu_args_to_modify'] as $arg){
            if (isset($record[$arg])){
            	$value=join(',',$record[$arg]);
                //echo "value=*$value*<br/>";
            	if ($value != '')
                  $modifs[] = "$arg =\"".str_replace("'","'",$value).'"';
                else
                  {
                  if (isset($record[$arg.'default'])){ //default value for file type input
                    $value=$record[$arg.'default'];
                    //echo "default=$value<br/>";
                    $modifs[] = "$arg =\"".str_replace("'","'",$value).'"';
                  }
                  }
            }   
        }
        $request = sprintf($pre_request,$_POST['fiu_table'],join(' , ',$modifs),$id);
        $request =  str_replace('"NULL"','NULL',$request);
        echo "<pre>$request</pre><br>";
        fiu_query($request);
    }
}

function fiu_insert(){ //GENERATION ET EXECUTION DES REQUETES D'INSERT (ZERO SECURITE)
    global $fiu_pdo;
    //show($_POST,'post in insert');
    fiu_anti_injection($_POST);
    //show($_POST,'post in insert after fiu_anti_injection');
    fiu_files_from_form();
    //show($_POST,'post in insert');
    $pre_request = 'INSERT INTO %s (%s) VALUES %s';
    //echo $pre_request;
    $new_data = fiu_flip(array_slice($_POST,3,count($_POST)-3)); // sauf fiu_table, fiu_args_to_modify et fiu_data_id
    //show($new_data,'newdata');
    $fields_list=implode(',',$_POST['fiu_args_to_modify']);
    //show($_POST['fiu_args_to_modify'],'arg to modify');
    //echo "$fields_list<br/>";
    $values=[];
    foreach ($new_data as $record){
    //show($record);
   	$value=[];
        foreach ($_POST['fiu_args_to_modify'] as $arg){
            if (!isset($record[$arg])){
                $value[] = 'NULL';
            }else{
                $la_valeur = join(',',$record[$arg]);
		if ($la_valeur == "''")
		  $la_valeur = 'NULL';
		$value[] = '"'.$la_valeur.'"';
            }
        }
        $values[]='('.join(',',$value).')';
    }
    $values=implode(',',$values);
    $request = sprintf($pre_request,$_POST['fiu_table'],$fields_list,$values);
    $request =  str_replace('"NULL"','NULL',$request);
    echo "<pre>$request</pre><br>";
    fiu_query($request);
}

/*============================================== aiguillage vers une exécution au chargement de ai.php ====================*/


if (isset($_POST['update'])){
    fiu_update();
}

if (isset($_POST['insert'])){
    fiu_insert();
}

/* ============================================= réponse ajax pour la pagination ====================  */

if (isset($_POST['pagination'])){

//fiu_view($_POST);
extract($_POST); // dangereux
    
//echo "offset=$offset avant<br/>";
switch ($button){
  case '|<':$offset = $offsetinitial;break;
  case '<<':$offset = max($offsetinitial,intdiv($offset,2));break;
  case '<': $offset = max($offsetinitial,$offset-$page);break;
  case '>': $offset = min($offset+$page,$limit-$page+1);break;
  case '>>': $offset = min($offset+intdiv($limit-$offset,2),$limit-$page+1);break;
  case '>|':$offset = $limit-$page+1;break; 
}

//echo 'q='.unserialize($q).'<br/>';
//echo "offset=$offset après<br/>";
preg_match('/^SELECT (.*?)(FROM.*?)(LIMIT(.*?))?(OFFSET(.*?))?$/',unserialize($q),$r);
//show($r);
$q = "SELECT $r[1]$r[2] LIMIT $page OFFSET $offset";
//echo "q=$q<br/>";
echo fiu($q,unserialize($mod),unserialize($m),unserialize($o),unserialize($e))."<div style='display:none' id='offcible$token'>$offset</div>";

//show($_POST);
exit;
}

/* -------------------------------- ajax return for recursive modals -------------------------------- */

if (isset($_GET['update_list'])){
    $f = $_GET['update_list'];
    //show($_GET,'GET');
    $fiu_recurs = $_GET['recurs'];
    $x = $_GET['x']+    50*($fiu_recurs)-250;

    $y = $_GET['y']+($fiu_recurs*50);
    //echo "<div class='in_modale_container' style='position:relative;top:".rand(1,200)."px;left:".rand(1,200)."px'>
    echo "<div class='in_modale_container' onkeypress='submitHisFormIfEnter(event,this);' style='position:relative;top:$y"."px;left:$x"."px'>
    <div class='close' onclick='fermeture(0)'>&times;</div><div class='in_modale'>";
    echo "<b>$fiu_recurs</b>".' '.sprintf(_("Add a record in table %s"),"<i>$f"."s</i><br>");

    echo fiu_sql2html("SELECT * FROM $f"."s LIMIT 1",'insert');
    echo "</div><div/>";
    exit;
}

// listbox options
if (isset($_GET['listbox'])){
    echo listbox_options($_GET['listbox']);
    exit;
    }

/* ---------------- for recursive input ---------------------------------------------*/

?>

<script>

function send_data(form){
    fd = new FormData(form);
    console.log(fd);
    var value = fd.get(fd.get('fiu_args_to_modify[]')+'[][]'); // value créée
    console.log("*"+value+"*");
    theValue[modal_level-1] = value;
    okToUpdate[modal_level-1] = true;
    console.log(theValue);
    ajax('','fiu.php',fd,'POST');
    fermeture(1);
}   


function submitHisFormIfEnter(event,div){ // Enter in the modal submit the form
        //console.log(event);
        if (event.code === "Enter"){
            event.preventDefault();
            //alert('ok');
            event.preventDefault();
            form = div.querySelector("form"); // the form in it
            send_data(form);
        }
    }

toUpdate=[]; // pour ajouter les tables dont il faudra metre à jour les menus déroulants
fId=[];

function isset (ref) { return typeof ref !== 'undefined' } // pour faire comme en php :)

function without_duplicate(a){
    return a.filter(function(ele,pos){ 
        return a.indexOf(ele) == pos;
    })     
}

// Thanks to https://stackoverflow.com/questions/7489982/how-to-get-the-index-of-an-option-in-a-select-menu-by-matching-the-text-with-pla
function findIndexfromOptionName( select, optionName ) {
    let options = Array.from( select.options );
    //console.log(options);
    console.log('*'+optionName+'*');
    $r = '';
    options.forEach((opt) => {
        console.log(opt.value+' '+opt.text);
        if (optionName === opt.text)
            $r = opt.value;
        });
    return $r;
}

function fermeture(update){ 
    if (modal_level == 0)
        return;
    var modal = document.getElementById('modal'+modal_level);
    modal_level = modal_level-1;
    modal.remove(); // on vire la modale
    if (update){
        console.log('listboxes update');
        //toUpdate = without_duplicate(toUpdate); // ok
        // mise à jour de toutes les listes déroulantes
        for(var i= 0; i < toUpdate.length; i++){
        // recupération de la listbox
        champ = toUpdate[i];
        ajax('temp','fiu.php?loadconfig=1&listbox='+champ); // on charge une fois seulement - economie d'ajax
        options = document.getElementById('temp').innerHTML; // if faudra modifier le id et le name etc...
        //console.log(options);
        // recupération des id des listes déroulantes à mettre à jour
        Array.from(document.getElementsByClassName('reload'+champ)).forEach(
            function(element) {        
                console.log('update reload='+element.id);
                select = Array.from(element.querySelectorAll('select'))[0]; // the select
                //console.log('-----------select---------');
                //console.log(select.innerHTML);
                if (element.id == reloadToSelect[modal_level]){ // changer le selected?
                    console.log(theValue[modal_level]);
                    select.innerHTML = options; // mets les nouvelles options
                    index = findIndexfromOptionName(select,theValue[modal_level]);
                    console.log('index='+index);
                    select.value = index;
                    console.log('nouvelle valeur='+select.value);
                    //console.log('il faut modifier au niveau '+modal_level+' '+reloadToSelect[modal_level]+' '+theValue[modal_level]);
                }
                else{
                    value = select.value; // récupère la valeur sélectionnée
                    select.innerHTML = options; // mets les nouvelles options
                    select.value = value; // remet la valeur sélectionnée
                }
            }
        )}
        console.log(reloadToSelect);
    }
}

var cursor_x = -1;
var cursor_y = -1;
document.onmousemove = function(event)
{
 cursor_x = event.pageX;
 cursor_y = event.pageY;
}

// create a div fiu_info (visible) and a div tmp (hidden) for ajax returns
function createInfoAndTemp(){
    if (document.getElementById('fiu_info') === null){
    newdiv = document.createElement('div');
    newdiv.id='fiu_info';
    document.body.appendChild(newdiv);
    newdiv = document.createElement('div');
    newdiv.id='temp';
    newdiv.style.display = 'none';
    document.body.appendChild(newdiv);    
    }
}
createInfoAndTemp(); 

/* event
   field: champ concerné pour les listbox
   idSelect: div à remplacer par les options
   fiu_id: id système (autoincrément pour les div)
   ??? recurs: niveau de récursion des modales - a priori inutile javascript est toujours à jour */

function modal(event,field,idSelect,fiu_token){
    console.log('idSelect='+idSelect);
    // search the actuel selected option (to replace back)
    //select = document.getElementById(idSelect); inutile ici
    //selected =  select.selectedIndex;
    //console.log('indexSelected= '+selected); // ok
    event.preventDefault();
    create_modale(); // add 1 to modal_level
    reloadToSelect[modal_level-1] = idSelect-1;
    ajax('modal'+modal_level,'fiu.php?update_list='+field+'&recurs='+(modal_level)+'&x='+cursor_x+'&y'+cursor_y+'&fiu_token='+fiu_token); // recurs is level of recursion 
    console.log('retour ajax');
    var m = document.getElementById('modal'+modal_level);
    //console.log(m.innerHTML);
    toUpdate[modal_level-1] = field; // listbox to update back
    m.style.display='inline-block'; 
}

function create_modale(){
    modal_level = modal_level+1; // initialized in fiu.js
    console.log("création de la modale "+modal_level);
    newdiv = document.createElement('div');
    newdiv.innerHTML = '<div id="modal'+modal_level+'" class="modal" style="display:inline-block"></div>';
    parent = document.getElementById('modal'+(modal_level-1));
    if (parent === null)
        document.body.appendChild(newdiv);
    else
        parent.appendChild(newdiv); 
}

// ne marche pas bien mais ferme
document.addEventListener('keydown', (event) => {
  if (event.key === 'Escape') {
    event.preventDefault();
    fermeture(0);
  }
})

// can be used in GET or POST, async or synchroneous
function ajax(div,url,data = null,method="GET",async = false) {
    var xmlhttp = new XMLHttpRequest();
    console.log('ajax: '+div+' '+url);
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) 
           if (xmlhttp.status == 200) 
                console.log('cible ajax: '+div);
                if (div !='')
                    document.getElementById(div).innerHTML = xmlhttp.responseText;
    };
    xmlhttp.open(method,url,async); // false makes the request synchronous but obsolete 
    xmlhttp.send(data);
}

</script>

<?php

//echo '<b>fiu</b> '.fiu_duration('fiu').'<hr>';
//echo fiu_metrix().'<hr>';   
?>
