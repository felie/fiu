// for paginate

modal_level = 0;
reloadToSelect = [];
toUpdate = []; // id of the select to fill with a value create by the modal
theValue = []; // the value create by form in the modal
okToUpdate = []; // ok to update at this level (the value - see above)

function submitForm(oFormElement,target) // for pagination
{
  var xhr = new XMLHttpRequest();
  var div = document.getElementById(target); // récupère l'id de la div où il faut injecter la réponse du serveur
  xhr.onreadystatechange = function(){ //  todo when ok (inject in the div the text)
        if (xhr.readyState ==4 && xhr.status == 200){
            div.innerHTML = xhr.responseText;
            forEach(document.getElementsByTagName('table'), function(table) { // pour rentre la table mise à jour sortable
                if (table.className.search(/\bsortable\b/) != -1) {
                    sorttable.makeSortable(table);
                }
            });
        }
    }
    //alert(oFormElement.action);
  xhr.open("POST", oFormElement.action, true); // ouvre le canal
  var data = new FormData(oFormElement);  // for modern browser... mybe for the others  serialize...
  console.log(document.activeElement);
  data.append('button',document.activeElement.getAttribute('b')); // get the value of the button clicked (<, >> etc...) ans add to the data
  data.append('offset',document.getElementById('off'+target).innerHTML);
  //for (var key of data.keys())
  //  console.log(key+'->'+data.get(key));
  xhr.send(data); // send the data to the server
}

function ajaxsendbyget(variable,value){
    var xhr = new XMLHttpRequest();
    xhr.open('GET','?action=setvariable&variable='+variable+'&value='+value);
    xhr.onload = function() {
        if (xhr.status === 200) {
             }
        else {
            alert('Request failed.  Returned status of ' + xhr.status);
        }
    }
    xhr.send();
}
 
/*
// make all input fields with type 'file' invisible thanks to Patrick Hillert at https://stackoverflow.com/questions/3226167/how-to-style-input-file-with-css3-javascript
$(':file').css({
  'visibility': 'hidden',
  'display': 'none'
});

// add a textbox after *each* file input
$(':file').after('<input type="text" readonly="readonly" value="" class="fileChooserText" /> <input type="button" value="Choose file ..." class="fileChooserButton" />');

// add *click* event to *each* pseudo file button
// to link the click to the *closest* original file input
$('.fileChooserButton').click(function() {
    $(this).parent().find(':file').click();
}).show();

// add *change* event to *each* file input
// to copy the name of the file in the read-only text input field
$(':file').change(function() {
    $(this).parent().find('.fileChooserText').val($(this).val());
});
*/