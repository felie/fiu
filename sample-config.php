<?php
// database
$ai_host = 'localhost';
$ai_db   = 'database';
$ai_user = 'user';
$ai_pass = 'password';
$ai_charset = 'utf8mb4';

$sortable = 1; // default for tables

// for developers
$fiu_debug = 0;
define('fiu_DEFAULT_ORIENTATION','h');
$fiu_label_ala_lisp = 0; // 1: nom(fondateur(ville)) 0: nom
$fiu_demo_mode = 0; 
$fiu_debug=3;
$fiu_dir_correction='doc';
$fiu_upload = 'upload';
$fiu_uploaddir = "/var/www/html/fiu/$fiu_upload";
$fiu_info=0;
$fiu_nb = 0;
?>
