<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('fiu_config.php');
require_once('fiu.php');
require_once('goodies/fiu_tools.php');

// accrochage de la session
//session_start(); 

?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Fiu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $fiu_header;?>
    <link rel="stylesheet" href="doc/style.css">
    <link rel="stylesheet" href="fiu.css">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="doc/movenotes/movenotes.css">
    <link rel="stylesheet" href="doc/menu/menu.css">
    <script src="doc/html2pdf.bundle.min.js"></script>
  </head>
<body class='<?php if (!isset($_SESSION['jn'])) $_SESSION['jn']='Jour';echo $_SESSION['jn']; ?>'> 

<?php

echo fiu_sql2html('SELECT duck,id_city_born FROM ducks LIMIT 3');

?>

<div id="modal"></div>

<div>
  <h1>Modal in pure JS</h1>

  <div id="triggerModal">
    <button modal="legal" >  
      Legal
    </button>
    <button modal="nouveaute" >  
      News
    </button>
    <button  modal="apropos" >
      About
    </button>
  </div>
</div>


<style>
.modal-shown {
  display: block;
}

.modal-hidden {
  display: none;
}

.modal {
    position: fixed; 
    z-index: 1; 
    padding-top: 100px;
    left: 0;
    top: 0;
    width: 100%; 
    height: 100%; 
    overflow: auto; 
    background-color: rgb(222,0,0);
    background-color: rgba(220,0,0,0.5); 
}

  /* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
    max-width: 960px;
}

.modal-close {

  cursor: pointer;
  right:0;
}

.modal-close:hover {
  box-shadow: 0 0 5px rgba(0,0,0,0.4);
}
</style>
<script>
const Content = {};

Content["legal"] = `
    <h2>Info Home</h2>
`

Content["nouveaute"] = `
    <h2>News</h2>
`

Content["apropos"] = `
    <h2>About</h2>
`

window.State = {
  content: null,
  isOpen: false,
  openDialog: function(key){
    State.content = Content[key]
    State.isOpen = true;
    view()
  },
  closeDialog: function(){
    State.isOpen = false;
    view()
  }
}

function view () {
  var modalCls =  State.isOpen 
    ? "modal modal-shown"  
    : "modal modal-hidden"
  modal.innerHTML =  `
    <div class="${modalCls}">
      <div class="modal-content">
        <div style="text-align:right" onclick="State.closeDialog()">
          <span class="modal-close">Close</span>
        </div>
        <p>${State.content}</p>
      </div>
    </div>
  `
}

triggerModal.addEventListener("click", function (e) {
  var modal  = e.target.getAttribute("modal")
  modal && State.openDialog(modal)
})

</script>
