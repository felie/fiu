<style>

/* for html2pdf to avoid cut div in two pages*/
/*div {page-break-inside: avoid}*/

.fiu_table{ 
  background:lightblue;  
}

th, td {
  border: 1px solid black;
  padding:3px;
}

.Jour th, .Jour td {
  border: 1px solid black;
  padding:3px;
}

.Nuit th, .Nuit td {
  border: 1px solid white;
  padding:3px;
}

th .vertical:before {
   line-height:30px; 
   width:110px !important;
}

th .vertical{
    /*float: left;*/
    /*writing-mode: sideways-lr;*/
    text-align: left;          
    padding:0px;
    width:auto;
    /*width:120px;*/
    line-height:28px;
    /*display:inline-block;*/
    vertical-align:middle;
    -webkit-transform: rotate(270deg);   
    /*-webkit-transform-origin: 50% 50%; */
    
}

table {
  border-collapse: collapse;
}

.fiu_table {
  border-collapse: collapse;
}

.Nuit td, .Nuit th {
    color:white;
}

.Jour td, .Jour th {
    color:black;
}

.Nuit th {
    background:#4d4d4d;
}

.noborder {
  border:0px !important; 
  }
  
.rightalign {
  text-align:right;
  }

input {
    background: #d5d7dd;
  }

select {
    background: #d5d7dd;
  }

/* for displaying array (when ai_demo=1) */
 
xmp { display:inline; }

/* simple css image gallery thanks to https://codepen.io/ran0904/pen/bNpLvX */
 
.gallery {
  width: 500px;
  margin: 0 auto;
  padding: 5px;
  background: #fff;
  box-shadow: 0 1px 1px rgba(0,0,0,.1);
}

.gallery > div {
  position: relative;
  float: left;
  padding: 5px;
}

.gallery > div > img {
  width:200px;
  transition: .1s transform;
  transform: translateZ(0); /* hack */
}

.gallery > div:hover {
  z-index: 1;
}

.gallery > div:hover > img {
  transform: scale(1.8,1.8);
  transition: .3s transform;
}

.cf:before, .cf:after {
  display: table;
  content: "";
  line-height: 0;
}

.cf:after {
  clear: both;
}

.biais { 
    -webkit-transform:rotate(-45deg);
    -moz-transform:rotate(-45deg);
    -o-transform:rotate(-45deg);
    -ms-transform:rotate(-45deg);  
    transform:  rotate(-45deg);     
}

/* for recursive modal */

.pseudo-button {
    display:inline-block;
    color:#444;
    border:1px solid #CCC;
    background:#EEE;
    box-shadow: 0 0 5px -1px rgba(0,0,0,0.2);
    cursor:pointer;
    vertical-align:middle;
    max-width: 100px;
    padding: 2px;
    border-radius: 6px;
    text-align: center;
}
pseudo-button:active {
    color:red;
    box-shadow: 0 0 5px -1px rgba(0,0,0,0.6);
}

.button_add{
  margin-left:-6px;
  display:inline;
}

/* recursive modal */

.modal{
    position:absolute;
    z-index:2;
    top:0;
    left:0;
    width:100vw;
    height:100vh;
    background-color: rgba(100,100,100,0.4);
}
.in_modale_container{
    border:0px solid blue;
    display:inline-block;
    box-shadow: 5px 5px 5px black;
    opacity: 1;
    background-color: lightblue;
}
.in_modale{
    opacity: 1;
    padding:30px;
}

/* The Close Button */
.close {
  margin-top:-8px;
  margin-right:-2px;
  display:inline;
  color: black;
  float: right;
  font-size: 32px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  text-decoration: none;
  cursor: pointer;
} 

/* ---------------- pagination -------------- */

.pagin{
  background:lightgray;
  padding:3px;
  margin:3px;
  border:0px red solid;
  display:inline-block;
  width:auto;
}

.image_pagin{
  margin:-2px;
  width:20px;
  height:20px;
}

.image_pagin:active{
  background:gray;
}

.button_pagin{
  background:transparent;
  padding:0;
  margin:0;
  width:0;
  height:0;
  border-style: solid;
  border:0;
}

</style>