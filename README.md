

# *Fiu*

*Fiu* is a php+mysql tool for developpers to save their time

*Fiu* provides mainly a dynamic function **fiu_sql2html** that produces html code in SELECT, INSERT and CREATE, with customized masks and envelop, and automatic mangagement of JOIN.

**fiu_sql2html** is an alias of the function **fiu** for those waho are very very fiu! (fiu is a tahitian word that means tired, spleen).

## Conventions

- Table names must end with an s
- All tables have a unique id field
- If the name of the table is 'foos', there must be a foo name field
- Foreign key field names are prefixed by id_ and possibly followed up by a name to discriminate two fields pointing to the same table (as id_duck_mother and id_duck_father)

## select, update, create 

### select

>fiu_sql2html('SELECT * FROM ducks LIMIT 2');

Return this:

![](images/image1.png)b

Turn the table ?

>fiu_sql2html('SELECT * FROM ducks LIMIT 2','select',[],'v');

Return this:

![](images/image2.png)

Note that here *mother* and *father* are foreign keys pointing at the ducks table itself, and city a fields pointing to another table! The joins were made by *Fiu* who knew nothing but the name of the table: ducks!

Obviously we can designate the fields we want to display.


>fiu_sql2html('SELECT duck,url FROM ducks');

Return this:

![](images/image3.png)

### update and create

Fiu is able to manage also UPDATE and CREATE queries, and the code produced is active to uptade or add data in database. See:
(in the this README.md, these are images, but you have a demo [fiu.elie.org](https://fiu.elie.org)

>fiu_sql2html('SELECT * FROM ducks LIMIT 2','update');

Return this:

![](images/image7.png)

>fiu_sql2html('SELECT * FROM ducks LIMIT 2','create');

Return this:

![](images/image11.png)

## Masks

### Principles

One can pass mask to the function, and...

>fiu_sql2html('SELECT duck,url FROM ducks LIMIT 2','select',['&lt;a href="%s">%s&lt;/a>']);

Return this:

![](images/image12.png)

### Predefined masks


>fiu_sql2html('SELECT duck,url FROM ducks LIMIT 2','select',['link']);


Return this:

![](images/image13.png)


>fiu_sql2html('SELECT duck,id_duck_mother FROM ducks LIMIT 2','select',['-','b']);

Return this:

![](images/image14.png)

###Affectation masks 

***

###Conditionnal masks

***

## Envelops

By default, the results are displayed as table, but you can define another envelop.


>fiu_sql2html('SELECT duck,id_duck_mother FROM ducks LIMIT 2','select',[],'h',['&lt;div style="display:inline-block;background:green">&lt;hr/>%s&lt;/div>','&lt;ul>%s&lt;/ul>','&lt;li>%s&lt;/li>','']);

<todo>

## Variables in masks and Envelops

## Tools to produce sql code to create database structure

## Mechanism of automatic joins

Let us resume our first request


>fiu_sql2html('SELECT * FROM ducks','select',[],'h',[],[],1);


Return this:

<p/>Parameters:
<center><table >
	<tr><td>REQUÊTE</td><td>SELECT * FROM ducks</td></tr>
    <tr><td>MODE   </td><td>select</td></tr>
    <tr><td>MASQUES</td><td>[]</td></tr>
    <tr><td>ORIENTATION</td><td>h</td></tr>
    <tr><td>ENVELOPPE</td><td>[]</td></tr> 
</table></center>
Calculated query:

SELECT ducks.id,ducks.duck,ALIAS143.city as 'born' ,ALIAS144.duck as 'mother' ,ALIAS145.duck as 'father' ,ducks.gender,ducks.clothes,ducks.url,ducks.image FROM ducks LEFT JOIN ducks as ALIAS145 ON ducks.id_duck_father = ALIAS145.id  LEFT JOIN ducks as ALIAS144 ON ducks.id_duck_mother = ALIAS144.id  LEFT JOIN citys as ALIAS143 ON ducks.id_city_born = ALIAS143.id 

Result herein after:

![](images/image16.png)

# Goodies

## graph of database

faire référence au "schéma de la base de données" dans adminer

<todo>

## automatic creation

<todo>

## pagination

<todo>

# LICENCE

GPLv3 AFERO






