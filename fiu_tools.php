<?php
/**
 * @copyright  Copyright (c) 2021 François Elie & Marin Elie
 * @license    http://opensource.org/licenses/AGPL-3.0 AGPL-3.0
 * @link       https://gitlab.adullact.org/felie/fiu
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);

session_start();
//echo 'session='.session_id().' dans fiu_tools<br>';
require_once('fiu_util.php');

function fiu_edit_file($filename){
    echo $filename;
    $content = file_get_contents($filename);
    echo "<form method='post' action='' enctype='multipart/form-data'>
    <input type='hidden' name='fiu_editfile'>
    <input type='hidden' name='filename' value='$filename'>
    <textarea name='text' cols=160 rows=30>$content</textarea>
    <input type='submit' value='ok'>
    </form>";
    exit;
}

//view($_POST,'post');
//view($_GET,'get');

if (isset($_POST['fiu_editfile'])){
    print_r($_POST);
    $text = $_POST['text'];
    $filename = $_POST['filename'];
    echo "filename=*$filename*<hr>$text<hr>";
    file_put_contents($filename,$text);
    header('Location: ?');
    exit;
}

if (isset($_GET['edit_file'])){
    echo fiu_edit_file($_GET['filename']);
    exit;
}

/* ------------ populate database ------------ 
 
    files in csv
    first line for fieldnames 
        id_target_alias create a record in the table target if not exists
        */
function get_zip(){ // populate tables with files in a zip
    
    return "<h3>Dépliage d'un zip en base de données</h3>
    
    <form method='post' action='' enctype='multipart/form-data'>
    <input type='hidden' name='fiu_unzip'/>
    <table>
        <tr><td colspan=2 align=right><input type='file' name='zip'/></td></tr>
    <tr class=noborder><td class=noborder colspan=2 align=right><input type='submit' value='ok'/></td></tr>
    </form>";
}

function fiu_unzip(){
    global $fiu_upload,$fiu_uploaddir;
    //show($_POST);
    extract($_POST);
    $mode = ($table2 == '') ? 'simple' : 'complex';
    $file=$_FILES['zip']['tmp_name'];
    //echo "file=".$file.'<br/>';
    $name=basename($file);
    $dir=dirname($file);
    $workdir="$fiu_uploaddir/$name".'ZIP';
    mkdir($workdir);
    if (move_uploaded_file($file,"$workdir/$name")){
        //echo 'déplacement du zip réussi<br/>';
        exec("cd $workdir;unzip $name"); // dezippage des fichiers
        unlink("$workdir/$name"); // effacement du fiu_zip
        $liste = glob("$workdir/*");
        //show($liste,'liste');
        switch($mode){
            case 'simple':
                foreach($liste as $path){
                    $fichier=basename($path);
                    $dir = "$fiu_upload/$table1";
                    $info = pathinfo($fichier);
                    $titre =  basename($fichier,'.'.$info['extension']);
                    $q="INSERT IGNORE INTO $table1"."s ($table1,url) VALUES('$titre','$dir/$fichier');";
                    if (!file_exists($dir)){
                        mkdir($dir);
                    }
                    rename($path,"$fiu_uploaddir/$table1/$fichier");
                    echo "q=$q<br/>";
                    fiu_query($q);
                    }
                break;
            case 'complex':
                echo "table2=$table2<br>";
                foreach($liste as $fichier){
                    $dir = "$fiu_upload/$table2";
                     if (!file_exists($dir)){
                        mkdir($dir);
                    }
                    $name=explode(' - ',basename($fichier));
                    rename($fichier,"$fiu_uploaddir/$table2/".basename($fichier));
                    $q="INSERT IGNORE INTO $table1"."s ($table1) VALUES ('$name[0]');"; // ajoute s'il n'est pas présent
                    echo "q = $q<br/>";
                    fiu_query($q);
                    $id1=fiu_simple_query("SELECT id FROM $table1"."s WHERE $table1='$name[0]';")[0];
                    $q="INSERT IGNORE INTO $table2"."s ($table2,id_$table1) VALUES ('$name[1]',$id1);"; // ajoute s'il n'est pas présent
                    echo "q = $q<br/>";
                    fiu_query($q);
                }
                    
            }
        }else{
    error(_('Échec du déplacement du zip'));
    }
}

/* ================================ action pour zip ===================================== */

if (isset($_POST['fiu_unzip'])){
    fiu_unzip();
}

/* ================== aide à la construction de base de données (MPD automatique) ======================================== */

function split_clever($s,$sep){
    $result=[];
    $temp='';
    $prof=0;
    for ($i=0;$i<strlen($s);$i++){
        $c=$s[$i];
        if ($c=='(' or $c=='{' or $c=='['){
            $prof++;
        }
        if ($c==')' or $c=='}' or $c==']'){
            $prof--;
        }
        if ($prof==0 and $c==$sep and $i>0){
            $temp=trim($temp);
            if ($temp!='')
                $result[]=$temp;
            $temp='';
        }else{
            $temp.=$c;
        }
    }
    $temp=trim($temp);
    if ($temp!='')
        $result[]=$temp;
    return $result;
}

// la fonction produit les requêtes sql de création et le schéma graphviz :)

function fiu_get_mpd(){
    return "
<form method='post' action='' enctype='multipart/form-data'>
  <input type='hidden' name='fiu_mpd'/>
    <table border=1 class='fiu_table'>
        <tr class=noborder><td class=noborder><textarea rows=7 cols=100 name='texte' value='votre description ici'>
class
pupil,id_pupil_ami,id_class,age SMALLINT 
demo,choice enum(cheese,dessert),clothes set(jean,hat,shirt)
teacher
subject
teacher_subject,id_teacher,id_subject,id_class
</textarea></td></tr>
        <tr class=noborder><td class=noborder align=right>"._("Add type in graph ?")."<input checked type=checkbox name='typeingraph'/></td></tr>
        <tr class=noborder><td class=noborder align=right>"._("Simple simulation ?")."<input type=checkbox name='simulation'/></td></tr>
        <tr class=noborder><td class=noborder align=right>"._("Populate with data ?")."<input type=checkbox name='populate'/></td></tr>
       <tr><td class=noborder align=right><input type='submit' value='ok'/></td></tr>
    </table>
    </form>";
}

function fiu_produce_generation_query($text,$populate=false){
    $t=explode("\n",trim($text));
    $query_creation='DROP TABLE IF EXISTS %1$s;CREATE TABLE %1$s (id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,%2$s) ENGINE=InnoDB;';
    $r='';
    $bidon=[];  
    $unknown=[];
    foreach ($t as $l){
        $l=trim($l);
        if ($l!=''){
            $chp='';
            $fields=[];
            $c=split_clever($l,',');
            $name = trim($c[0]).'s';
            $dummyfield=[];
            $n = 0;
            foreach ($c as $f){
                if (substr($f,0,3) == 'id_'){
                    $chp =" INT UNSIGNED DEFAULT 1";
                    $fields[] = "  $f $chp";
                    $net=str_replace('_','',$f);
                    $x=explode('_',$f);
                    $dummyfield[]=$f;
                    $fields[]="  CONSTRAINT $name"."_fk_$f FOREIGN KEY ($f) REFERENCES $x[1]"."s(id)";
                }else{
                    if (preg_match('/^(.*) (.*)$/U',$f,$rp)){
                        $rp[2]=str_replace(array('(',',',')'),array('("','","','")'),$rp[2]);
                        $chp=" $rp[2]";
                        $fields[]="  $rp[1]$chp";
                    }else{ // no defined type
                        $chp=" TINYTEXT";
                        $fields[]="$f$chp";
                        if ($n == 0) // for index, it should be a varchar
                            $fields[]="UNIQUE KEY `idx_$f` (`$f`(50))";
                        else
                            $fields[]="KEY `idx_$f` (`$f`(50))";
                        $dummyfield[]=$f;
                        $champ=$f;
                    }
                }
                echo "champ $f avec n=$n<br>";
                $n++;
            }
        }
        $fs = implode(",",$fields);
        $r.=trim(sprintf($query_creation,$name,$fs)); 
        $unknown[]="INSERT IGNORE INTO $name($c[0]) VALUES (NULL);";
        if ($populate){
            for($i=1;$i<11;$i++){
                $value=[];
                foreach($dummyfield as $d){
                    $value[]=random_int(1,10);
                }
                $value[0]="'$champ $i'";
                $bidon[] = "INSERT IGNORE INTO $name(".join(',',$dummyfield).") VALUES (".join(',',$value).");";
            }
        }
    }
    // to avoid contraint error during creation of data;s
    $result = "SET FOREIGN_KEY_CHECKS=0;";
    $result .= $r;
    //show($bidon,'bidon');    
    if ($populate)
        $result .= join('',$bidon);
    //$result .= join('',$unknown); //bidouille inutile si les champs peuvent être null ???
    $result .= "SET FOREIGN_KEY_CHECKS=1;";
    //echo "$result<br>";
    return $result;
}

function fiu_array2html($a){
    if ($a!=[]){
        $r='<table class="fiu_table">';
        foreach ($a as $line){
            $r.='<tr>';
            if (is_array($line)){
                foreach ($line as $c){
                   $r.="<td>$c</td>";
                }
            }else{
                $r.="<td>$line</td>";
            }
        }
        return $r.'</table>';
    }
}

/* ------------------------- for graphviz --------------------*/

function fiu_graphviz2(){ // thanks to Tanguy Morlier  see https://sergio.24eme.fr/2022/04/04/mysql-graphviz-schema/ for dependencies
    global 
    $fiu_host,$fiu_db,$fiu_user,$fiu_pass,$fiu_temp,$fiu_path;
    //echo "fiu_path=$fiu_path<br>";
    $command="cd $fiu_path/temp;sadisplay -u \"mysql://$fiu_user:$fiu_pass@$fiu_host/$fiu_db\" > schema.dot;dot -Tpng schema.dot > schema.png";
    //echo $command;
    exec($command);
    return '<img src="temp/schema.png?'.rand(1,1000).'">';
}

function fiu_sql_structure() // get the database structure
	{
	$tables = fiu_simple_query('SHOW TABLES');
    $r = [];
    foreach($tables as $table)
        $r[] = fiu_total_query("SHOW CREATE TABLE $table")[0]['Create Table']."\n";
    return $r;
    }

function fiu_graphviz($structure,$typeingraph=true){
    global $fiu_path;
    $graph="digraph G
    {
        rankdir = LR;
        %s
        %s
        bgcolor = transparent
        }";
        $node='%s
        [   
            shape = none 
            label = <<table border="0" cellspacing="0">
            %s
            </table>>
            ]';
    $links = [];
    foreach($structure as $definition_table){
        $lines = [];
        foreach(explode("\n",$definition_table) as $line){
            if ($line[0] == 'C'){ // CREATE
                $line = explode('`',$line);
                $table = $line[1];
                //echo "<b>$table</b><br>";
                $lines[]="<tr><td port=\"$table\" border='1' bgcolor='goldenrod'><B>$table</B></td></tr>";
            }
            elseif ($line[2] == '`'){ // field description
                $line = explode('`',$line);
                $field = $line[1];
                $net = str_replace('_','',$field);
                $type = substr($line[2],0,-1);
                //echo "<i>$field<i> $type<br>";
                if ($typeingraph)
                    $field ="$field $type";
                $lines[]="<tr><td align='left' port=\"$net\" border='1' bgcolor='gold'>$field</td></tr>";
            }
            elseif ($line[2] == 'C'){ // CONSTRAINT
                $line = explode('`',$line);
                $field = $line[3];
                $net = str_replace('_','',$field);
                $target = explode('_',$field)[1]; // target class in id_class id_class_alias
                //echo "foreign key <i>$field</i> -> $target<br>";
                $links[]="$table:$net -> $target".'s:id';
            }
        }
        $g.=sprintf($node,$table,implode("\n",$lines))."\n";
    }
    $g=sprintf($graph,$g,implode("\n",$links));
    //echo "<xmp>$g</xmp>";
    file_put_contents("$fiu_path/temp/graph.txt",$g);
    $r = rand(1,100000);
    exec("cd $fiu_path/temp;dot -Tpng graph.txt > graph.png");
    unlink("$fiu_path/temp/graph.txt");
    return "<img src='temp/graph.png?$r' style='scale:50%'>";
}


function fiu_drop_tables(){ // erase all data of the database - be carefull...
    global $fiu_pdo;
    $tables = fiu_simple_query("SHOW TABLES");
    $r = '';
    foreach ($tables as $table)
        $r .="DROP TABLES $table;";
    fiu_query(sprintf("SET FOREIGN_KEY_CHECKS=0;%sSET FOREIGN_KEY_CHECKS=1;",$r));
}


/* ================================== action (MPD) =============================================== */

if (isset($_POST['fiu_mpd'])){
    $typeingraph = isset($_POST['typeingraph']);
    $simulation = isset($_POST['simulation']);
    $populate = isset($_POST['populate']);
    fiu_start_the_timer('generation');
    $queries=fiu_produce_generation_query($_POST['texte'],$populate);
    if (!$simulation)
        {
        $result .= _('Exécution des requêtes').'<br>';
        fiu_query($queries);
        }
    echo 'generation '.fiu_duration('generation');
    $result .= "<p/>"._("Tables in your database:")." <b>$fiu_db</b><br/>";
    $result .= fiu_array2html(fiu_simple_query('SHOW TABLES'));
    $result .= fiu_graphviz(fiu_sql_structure(),$typeingraph);
    $result .= fiu_graphviz2();
    $result.="<p/>"._("What the generator wrote for you:")."

```
    $queries
    
```
    
";
}

?>
